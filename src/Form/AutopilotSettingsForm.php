<?php

/**
 * @file
 * Contains \Drupal\autopilothq\Fprm\AutopilotSettingsForm
 */

namespace Drupal\autopilothq\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Autopilot settings.
 */
class AutopilotSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'autopilothq_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      'autopilothq.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('autopilothq.settings');

    $form['trackingId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tracking ID'),
      '#default_value' => $config->get('trackingId'),
    ];

    $form['app'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use App code'),
      '#description' => $this->t('Only use this if your Drupal installation is for an application and not a website.'),
      '#default_value' => $config->get('app'),
    ];

    $form['apiKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('apiKey'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('autopilothq.settings')
      ->set('trackingId', $form_state->getValue('trackingId'))
      ->set('app', $form_state->getValue('app'))
      ->set('apiKey', $form_state->getValue('apiKey'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}